######## 3_ FIRST FUNCTIONS ######## 

# Leap years
is_leap = function(a) {
  return(ifelse(((a %% 4 == 0) & (a %% 100 != 0)) | ((a %% 4 == 0) & (a %% 100 == 0) & (a %% 400 == 0)), "Leap", "Not leap"))
}


# Roots
roots = function(a, b, c) {
  delta <- b**2 - (4 * a * c)
  if (delta == 0) {
    root = -b/(2 * a)
    return(list(case = 0, r1 = root))
  } else if (delta > 0) {
    root1 = (-b - sqrt(delta))/(2 * a)
    root2 = (-b + sqrt(delta))/(2 * a)
    return(list(case = 1, r1 = root1, r2 = root2))
  } else if (delta < 0) { return(list(case = -1)) }
}

a <- 5
b <- 2
c <- -100
result <- roots(a, b, c)

f = function(x) { a*x**2 + b*x + c}
curve(f, from = -10, to = 10) # 2 roots


######## 4_ VECTORS ######## 

# Usual functions
v1 <- c(3,8,25,4,9,1.5,71,20,5,0.02)
length(v1)
mode(v1) # numeric

v2 <- 1:15
v3 <- seq(0.1, 0.5, by = 0.1)
v4 <- seq(50, 90, length = 100)


# Operations on vectors
5 * v1
5 * v1 + 10
v1 > 5
v1 + v2

# ...

# Manipulating vectors
n <- length(v1)
mu = mean(v1)
sigma2 <- (1/(n - 1)) * sum((v1 - mu)**2) # = 457.3822
var(v1) # = 457.3822


######## 5_ FUNCTIONS ######## 

######## 6_ DATA TABLE ######## 

# The read.table() function
tab <- read.table("donnees-individus.txt", h = T, sep = "\t")
summary(tab)
str(tab)


# Exploration of the table
dim(tab) # 207 lines 5 columns
names(tab) # 5 variables


# Columns selection
mean(tab$poids) # = 61.86155

mean(tab$taille[tab$genre == "femme"]) # = 164.6885
mean(tab$taille[tab$genre == "homme"]) # = 173.1079

length(which(tab$genre == "femme" & tab$taille > 170 & tab$yeux == "bleu"))# 6 people

mean(tab$poids[tab$genre == "homme"]) # = 65.45417

mean(tab$poids[tab$genre == "homme" & tab$lunettes == "oui"]) # = 65.045

min(tab$poids[tab$genre == "homme"]) # = 46.68
which(tab$poids == 46.68) # 138
tab[138, ]

tab$imc <- tab$poids/(tab$taille)**2
summary(tab)


# First graphs
hist(tab$taille, xlab = "Height", col = 4)
boxplot(tab$taille ~ tab$genre, xlab = "Gender", ylab = "Height")
plot(tab$taille, tab$poids, xlab = "Height", ylab = "Weight", main = "Weight ~ Height")

library(dplyr)
